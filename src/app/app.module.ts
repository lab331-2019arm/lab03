import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule} from '@angular/forms';
import{HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl.service';
import { from } from 'rxjs';
import { StudentFileImplService } from './service/student-file-impl.service';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [
    {provide: StudentService, useClass: StudentFileImplService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
