import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import { Student } from '../entity/student';
@Injectable({
  providedIn: 'root'
})
export class StudentDataImplService extends StudentService {

  constructor() {
    super();
  }
  getStudents(): Observable<Student[]> {
    return of(this.students);
  };

  students: Student[] = [{
    id: 1,
    studentId: '562110507',
    name: 'Prayuth',
    surname: 'Tu',
    gpa: 4.00,
    image:'assets/images/tu.jpg',
    featured: true,
    penAmount:1,
    description:"5546515615615"
  }, {
    id: 2,
    studentId: '562110509',
    name: 'Pu',
    surname: 'Priya',
    gpa: 1.25,
    image:'assets/images/pu.jpg',
    featured: false,
    penAmount:11,
    description:"5546515615615"
  },
  {
    id: 3,
    studentId: '562110509',
    name: 'Oreo',
    surname: 'German',
    gpa: 2.6,
    image:'assets/images/oreo.jpg',
    featured: true,
    penAmount:111,
    description:"5546515615615"
  }


];
}
