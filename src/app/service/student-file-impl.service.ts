import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../entity/student';
import { StudentService } from './student-service';

@Injectable({
  providedIn: 'root'
})
export class StudentFileImplService extends StudentService {
  

  
  constructor(private http: HttpClient) {
    super();
   }
   getStudents():Observable<Student[]>{
     return this.http.get<Student[]>('assets/student.json')
   }
}

